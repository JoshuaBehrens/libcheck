//  Copyright (c) 2014 Joshua Behrens
//
//  This software is provided 'as-is', without any express or implied
//  warranty. In no event will the authors be held liable for any damages
//  arising from the use of this software.
//
//  Permission is granted to anyone to use this software for any purpose,
//  including commercial applications, and to alter it and redistribute it
//  freely, subject to the following restrictions:
//
//     1. The origin of this software must not be misrepresented; you must not
//     claim that you wrote the original software. If you use this software
//     in a product, an acknowledgment in the product documentation would be
//     appreciated but is not required.
//
//     2. Altered source versions must be plainly marked as such, and must not be
//     misrepresented as being the original software.
//
//     3. This notice may not be removed or altered from any source
//     distribution.
//
//  Based on the zlib-license: http://zlib.net/zlib_license.html
//

#include <string>
#include <vector>
#include <fstream>
#include <map>

// minizip header files
#include "zip.h"
#include "unzip.h"

class Zip
{
public:
	Zip( )
	{
		internal = 0;
		open = false;
		open2 = false;
	}
	~Zip( )
	{
		Close( );
	}

	bool Open( const std::string& filename, const bool& append = false )
	{
		Close( );
		internal = zipOpen( filename.c_str( ), append ? APPEND_STATUS_ADDINZIP : APPEND_STATUS_CREATE );
		return open = internal != 0;
	}

	bool OpenCurrent( const std::string& contextName )
	{
		if ( open )
		{
			CloseCurrent( );
			return open2 = zipOpenNewFileInZip( internal, contextName.c_str( ), 0, 0, 0, 0, 0, 0, Z_DEFLATED, Z_DEFAULT_COMPRESSION ) == ZIP_OK;
		}
		return false;
	}
	bool WriteCurrent( const void* buffer, const std::size_t& bufferLen = 0 )
	{
		if ( open2 )
			return zipWriteInFileInZip( internal, buffer, bufferLen ) == ZIP_OK;
		return false;
	}
	bool WriteCurrent( const std::vector<char>& data )
	{
		return WriteCurrent( &data[0], data.size( ) );
	}
	void CloseCurrent( )
	{
		if ( open2 )
		{
			zipCloseFileInZip( internal );
			open2 = false;
		}
	}
	void Close( )
	{
		CloseCurrent( );
		if ( open )
		{
			zipClose( internal, 0 );
			open = false;
		}
	}
private:
	Zip( const Zip& ) { }
	Zip& operator = ( const Zip& ) { return *this; }

	zipFile internal;
	bool open, open2;
};

class Unzip
{
public:
	Unzip( )
	{
		internal = 0;
		open = false;
		open2 = false;
	}
	~Unzip( )
	{
		Close( );
	}

	bool Open( const std::string& filename )
	{
		Close( );
		internal = unzOpen( filename.c_str( ) );

		if ( internal != 0 )
		{
			unzGoToFirstFile( internal );
			do
			{
				unz_file_info info;
				char contextName[ 256 ];
				if ( unzGetCurrentFileInfo( internal, &info, contextName, 256, 0, 0, 0, 0 ) == UNZ_OK )
					sizes.insert( std::make_pair( std::string( contextName ), info.uncompressed_size ) );
			} while ( unzGoToNextFile( internal ) != UNZ_END_OF_LIST_OF_FILE );

			return open = true;
		}
		return open = false;
	}

	bool OpenCurrent( const std::string& contextName )
	{
		if ( open )
		{
			CloseCurrent( );

			if ( unzLocateFile( internal, contextName.c_str(), 0 ) == UNZ_OK && unzOpenCurrentFile( internal ) == UNZ_OK )
			{
				unzSetOffset( internal, 0 );
				return open2 = true;
			}
		}
		return open = false;
	}
	bool ReadCurrent( char * buffer, std::size_t& bufferLen )
	{
		if ( open2 )
		{
			int ret = unzReadCurrentFile( internal, buffer, bufferLen );
			if ( ret >= 0 )
			{
				bufferLen = ret;
				return true;
			}
		}
		return false;
	}
	bool ReadCurrent( std::vector<char>& data )
	{
		char buf;
		std::size_t t = 1;
		std::size_t old = data.size( );
		while ( ReadCurrent( &buf, t ) )
			data.push_back( buf );
		return data.size( ) != old;
	}
	void CloseCurrent( )
	{
		if ( open2 )
		{
			unzCloseCurrentFile( internal );
			open2 = false;
		}
	}
	void Close( )
	{
		CloseCurrent( );
		if ( open )
		{
			unzClose( internal );
			sizes.clear( );
			open = false;
		}
	}

	typedef std::map< std::string, std::size_t >::const_iterator file_iterator;
	file_iterator begin_files( ) const { return sizes.begin( ); }
	file_iterator end_files( ) const { return sizes.end( ); }
private:
	Unzip( const Unzip& ) { }
	Unzip& operator = ( const Unzip& ) { return *this; }

	std::map< std::string, std::size_t > sizes;

	unzFile internal;
	bool open, open2;
};
