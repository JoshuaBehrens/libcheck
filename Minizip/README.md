### Simple Minizip Wrapper ###

This wrapper is just a small, functional but cheap example how you could use the [Minizip library](www.winimage.com/zLibDll/minizip.html). If you want to use this you should improve the error checking.

### Setup ###

* Copy source from this
* Requirements
      * [Minizip](www.winimage.com/zLibDll/minizip.html) - *zlib license*
      * related [zlib](www.zlib.net) - *zlib license ... as you could have guessed*
* Take a look at the examples

### Zip Example ###

```
    zip up a file

	Zip wrap;
	if ( wrap.Open( "container.zip" /*, add files to existing behavior? */ ) ) // create a container
	{
		if ( wrap.OpenCurrent( "fileToCompress.txt" ) ) // give it a name
		{
			std::string content = "File content.";
			if ( !wrap.WriteCurrent( content.c_str( ), content.size( ) ) )
				// oh no, something unexpected happened
		}
		// else // oh no, something unexpected happened
	}
	// else // oh no, something unexpected happened
    
    // hooray everything gets automatically closed
```

### Unzip Example ###

```
	Unzip wrap;
	if ( wrap.Open( "container.zip" ) ) // open the container
	{
		for ( Unzip::file_iterator it( wrap.begin_files( ) ) ; it != wrap.end_files( ) ; ++it ) // iterate over all read files
		{
			std::size_t buflen = 256;
			char buf[ 256 ];
			std::ofstream out( it->first.c_str( ), std::ios_base::binary );

			if ( wrap.OpenCurrent( it->first ) )
                while ( wrap.ReadCurrent( buf, buflen ) && buflen > 0 )
                    out.write( buf, buflen );
            // else // oh no, something unexpected happened
		}
	}
	// else // oh no, something unexpected happened
    
    // hooray everything gets automatically closed
```

### License ###

Copyright (c) 2014 Joshua Behrens

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

Based on the zlib-license: [http://zlib.net/zlib_license.html](http://zlib.net/zlib_license.html)

### tl;dr ###

As long as you do NOT say you wrote it and NOT remove this hint, you can use it non- and commercially. But if anything fails I am not the one you should blame.

As this repository contains just simple source and is nothing too serious I just don't want you say you wrote it and you can forget about the acknowledgement in §1. 